package com.algorithm.interview;

public class GreatestCommonDivisorV2 {
    /**
     * Euclidean algo
     * time complexity is about O(log(max(a,b)))
     * % operarion performance is bad
     * @param a
     * @param b
     * @return
     */
    public static int getGreatestCommonDivisor(int a, int b){
        if (a==b){
            return a;
        }
        if ((a&1)==0 && (b&1)==0){ //both even
            return getGreatestCommonDivisor(a>>1,b>>1)<<1;
        } else if ((a&1)==0 && (b&1)!=0) {//a is even, b is odd
            return getGreatestCommonDivisor(a>>1, b);
        } else if ((a&1)!=0 && (b&1)==0) {
            return getGreatestCommonDivisor(a,b>>1);
        } else {//both odd
            int big = a>b ? a:b;
            int small = a<b ? a:b;
            int c = big - small;
            return getGreatestCommonDivisor(c, small);
        }
    }

    public static void main(String[] args) {
        System.out.println(getGreatestCommonDivisor(25,5));
        System.out.println(getGreatestCommonDivisor(100,80));
        System.out.println(getGreatestCommonDivisor(27,14));
    }
}
