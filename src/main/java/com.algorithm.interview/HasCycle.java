package com.algorithm.interview;

public class HasCycle {
    /**
     * time complexity: O(n)
     * space complexity: O(1)
     * @param head
     * @return
     */
    public static boolean isCycle(Node head){
        Node p1 = head;
        Node p2 = head;
        while (p2!=null && p2.next!=null){
            p1 = p1.next;
            p2 = p2.next.next;
            if (p1==p2){
                return true;
            }
        }
        return false;
    }

    private static class Node{
        int data;
        Node next;
        Node(int data){
            this.data = data;
        }
    }

    public static void main(String[] args) {
        Node n1 = new Node(5);
        Node n2 = new Node(2);
        Node n3 = new Node(3);
        Node n4 = new Node(7);
        Node n5 = new Node(6);
        n1.next = n2;
        n2.next = n3;
        n3.next = n4;
        n4.next = n5;
        n5.next = n3;
        System.out.println(isCycle(n1));
    }
}
