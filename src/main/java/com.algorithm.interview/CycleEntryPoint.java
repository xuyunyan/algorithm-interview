package com.algorithm.interview;

public class CycleEntryPoint {
    /**
     * time complexity: O(n)
     * space complexity: O(1)
     * calculate the cycle length
     * @param head
     * @return
     */
    public static Node isCycle(Node head){
        Node p1 = head;
        Node p2 = head;
        while (p2!=null && p2.next!=null){
            p1 = p1.next;
            p2 = p2.next.next;

            if (p1==p2){
                int length = 0;
                p2 = head;
                while (p2!=null && p2.next!=null){
                    p1 = p1.next;
                    p2 = p2.next;
                    length++;
                    if (p1==p2){
                        return p1;
                    }
                }
            }
        }
        return head;
    }

    private static class Node{
        int data;
        Node next;
        Node(int data){
            this.data = data;
        }
    }

    public static void main(String[] args) {
        Node n1 = new Node(5);
        Node n2 = new Node(2);
        Node n3 = new Node(3);
        Node n4 = new Node(7);
        Node n5 = new Node(6);
        n1.next = n2;
        n2.next = n3;
        n3.next = n4;
        n4.next = n5;
        n5.next = n3;
        System.out.println(isCycle(n1).data);
    }
}
