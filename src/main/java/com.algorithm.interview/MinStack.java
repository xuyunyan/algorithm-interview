package com.algorithm.interview;

import java.util.Stack;

public class MinStack {

    Stack<Integer> mainStack = new Stack<>();
    Stack<Integer> minStack = new Stack<>();

    public void push(Integer i){
        mainStack.push(i);
        if (minStack.isEmpty()){
            minStack.push(i);
        }else{
            if (minStack.peek()>i){
                minStack.push(i);
            }
        }
    }

    public Integer pop() throws Exception{
        if (mainStack.isEmpty()){
            throw new Exception("stack is empty!");
        }
        if (mainStack.peek().equals(minStack.peek())){
            minStack.pop();
        }
        return mainStack.pop();
    }

    public Integer getMin() throws Exception{
        if (mainStack.isEmpty()){
            throw new Exception("stack is empty!");
        }
        return minStack.peek();
    }

    public static void main(String[] args) throws Exception{
        MinStack m = new MinStack();
        m.push(4);
        m.push(9);
        m.push(7);
        m.push(3);
        m.push(5);
        System.out.println(m.getMin());
        System.out.println(m.pop());
        System.out.println(m.pop());
        System.out.println(m.getMin());
    }

}
