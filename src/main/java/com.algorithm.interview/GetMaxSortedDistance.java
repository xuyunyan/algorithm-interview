package com.algorithm.interview;

public class GetMaxSortedDistance {
    public static int getMaxSortedDistance(int[] array){
        int max = array[0];
        int min = array[0];
        for (int i : array){
            if (i>max){
                max = i;
            }
            if (i<min){
                min = i;
            }
        }
        int d = max-min;
        if (d==0) {
            return 0;
        }

        int bucketNum = array.length;
        Bucket[]  buckets = new Bucket[bucketNum];
        for (int i=0; i<bucketNum; i++){
            buckets[i] = new Bucket();
        }
        //input to the buckets
        for (int i=0; i<array.length; i++){
            int index = (array[i]-min)*(bucketNum-1)/d;
//            int index = (array[i]-min)/d*(bucketNum-1);//why it's not work?--because /d already =0.
            if (buckets[index].min==null || buckets[index].min>array[i]){
                buckets[index].min=array[i];
            }
            if (buckets[index].max==null || buckets[index].max<array[i]){
                buckets[index].max=array[i];
            }
        }
        //forloop buckets and find the max distance
        int leftMax = buckets[0].max;
        int maxDistance = 0;
        for (int i=1; i<buckets.length; i++){
            if (buckets[i].min == null){
                continue;
            }
            if ((buckets[i].min - leftMax)>maxDistance){
                maxDistance=buckets[i].min-leftMax;
            }
            leftMax = buckets[i].min;
        }

        return maxDistance;
    }

    private static class Bucket {
        Integer min;
        Integer max;
    }
    public static void main(String[] args) {
        int[] array = new int[] {2,6,3,4,5,10,9};
        System.out.println(getMaxSortedDistance(array));
        int d = (3-2)/8*6;
        System.out.println(d);
        int e = (3-2)*6/8;
        System.out.println(e);
    }
}
