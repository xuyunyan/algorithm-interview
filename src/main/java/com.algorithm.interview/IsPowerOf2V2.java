package com.algorithm.interview;

public class IsPowerOf2V2 {
    /**
     * time complexity O(1)
     * @param num
     * @return
     */
    public static boolean isPowerOf2(int num){
        int temp = 1;
        while (temp<=num){
            if (temp==num){
                return true;
            }
            temp = temp <<1;
        }
        return false;
    }

    public static void main(String[] args) {
        System.out.println(isPowerOf2(18));
        System.out.println(isPowerOf2(32));
    }
}
