package com.algorithm.interview;

public class IsPowerOf2V3 {
    /**
     * force forloop
     * time complexity O(logn)
     * @param num
     * @return
     */
    public static boolean isPowerOf2(int num){
        int temp = num-1;
        if ((num & temp) ==0){
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        System.out.println(isPowerOf2(18));
        System.out.println(isPowerOf2(32));
    }
}
