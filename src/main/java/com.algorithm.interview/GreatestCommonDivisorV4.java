package com.algorithm.interview;

public class GreatestCommonDivisorV4 {
    /**
     * combine Euclidean and 九章算术方法
     * time complexity worst case: O(log(max(a,b)))
     * @param a
     * @param b
     * @return
     */
    public static int getGreatestCommonDivisor(int a, int b){
        int big = a>b ? a : b;
        int small = a<b ? a:b;
        if (big%small==0){
            return small;
        }
        int c = big-small;
        return getGreatestCommonDivisor(b,c);
    }

    public static void main(String[] args) {
        System.out.println(getGreatestCommonDivisor(25,5));
        System.out.println(getGreatestCommonDivisor(100,80));
        System.out.println(getGreatestCommonDivisor(27,14));
    }
}
